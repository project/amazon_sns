<?php

namespace Drupal\Tests\amazon_sns\Kernel;

use Drupal\amazon_sns\Controller\NotificationController;
use Drupal\amazon_sns\Event\SnsEvents;
use Drupal\amazon_sns\Event\SnsNotificationSubscriber;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\amazon_sns\Unit\PlainTextMessageTrait;
use Symfony\Component\ErrorHandler\BufferingLogger;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tests the notification controller and error handling.
 *
 * @group amazon_sns
 */
class SnsNotificationSubscriberTest extends KernelTestBase {
  use PlainTextMessageTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'amazon_sns',
  ];

  /**
   * Test logging notifications when enabled.
   */
  public function testLogNotification() {
    $this->mockGuzzleCertRequest();
    $config = $this->config('amazon_sns.settings');
    $config->set('log_notifications', TRUE);
    $config->save();

    $logger = new BufferingLogger();
    $this->container->get('logger.factory')->addLogger($logger);
    $controller = NotificationController::create($this->container);
    $request = Request::create('http://example.com/_amazon-sns/notify', 'POST', [], [], [], $this->getFixtureServer(), $this->getFixtureBody());
    $controller->receive($request);
    $message = $logger->cleanLogs()[0];
    $this->assertEquals(RfcLogLevel::INFO, $message[0]);
    $this->assertEquals("Notification %message-id received for topic %topic.", $message[1]);
    $this->assertEquals('9438aee6-d476-5e20-ba25-ff24bf09d6ce', $message[2]['%message-id']);
    $this->assertEquals('arn:aws:sns:us-west-2:604091128280:testing1', $message[2]['%topic']);
    $this->assertEquals('amazon_sns', $message[2]['channel']);
  }

  /**
   * Test that nothing is logged when it's disabled.
   */
  public function testLogDisabled() {
    $this->mockGuzzleCertRequest();
    $config = $this->config('amazon_sns.settings');
    $config->set('log_notifications', FALSE);
    $config->save();

    $logger = new BufferingLogger();
    $this->container->get('logger.factory')->addLogger($logger);
    $controller = NotificationController::create($this->container);
    $request = Request::create('http://example.com/_amazon-sns/notify', 'POST', [], [], [], $this->getFixtureServer(), $this->getFixtureBody());
    $controller->receive($request);
    $this->assertEmpty($logger->cleanLogs());
  }

  /**
   * Test that our logger is first in the service container.
   *
   * Note that if a site defines another subscriber with 100 priority, ours may
   * still not be first.
   */
  public function testLoggerIsFirst() {
    $dispatcher = $this->container->get('event_dispatcher');

    // Add a dummy listener at the default priority.
    $dispatcher->addListener(SnsEvents::NOTIFICATION, function () {});

    $listeners = $dispatcher->getListeners(SnsEvents::NOTIFICATION);
    $this->assertInstanceOf(SnsNotificationSubscriber::class, $listeners[0][0]);
  }

}
